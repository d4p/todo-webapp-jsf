<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<title>Formularz rejestracji</title>
</head>
<div class="container" style="width: 300px">
<form method="POST" action="register">
<h2 class="form-signin-heading">Rejestracja:</h2>
<input type="text" class="form-control" placeholder="Nazwa użytkownika" required autofocus name="login" /><br />
<input type="password" class="form-control" placeholder="Hasło" required name="password1" /> <br />
<input type="password" class="form-control" placeholder="Powtórz hasło" required name="password2" /><br />
<button class="btn btn-lg btn-primary btn-block" type="submit">Zarejestruj się</button>
</form>
</div>
</body>
</html>