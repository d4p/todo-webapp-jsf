package pl.edu.pjwstk.gdansk.s10908.jaz.todo.web;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.User;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.service.AuthenticationService;

@RequestScoped
@Named("login")
public class LoginController {

    @EJB
    private AuthenticationService authenticationService;

    private User currentUser;

    private User userDTO;

    @PostConstruct
    public void init() {
	currentUser = authenticationService.getCurrentUser();
	if (currentUser == null) {
	    userDTO = new User();
	} else {
	}
    }

    public User getCurrentUser() {
	return currentUser;
    }

    public void setCurrentUser(User currentUser) {
	this.currentUser = currentUser;
    }

    public User getUserDTO() {
	return userDTO;
    }

    public boolean login(String login, String password) {
	userDTO = authenticationService.login(login, password);
	if (userDTO != null) {
	    return true;
	} else {
	    return false;
	}
    }

    public void setUserDTO(User userDTO) {
	this.userDTO = userDTO;
    }

}
