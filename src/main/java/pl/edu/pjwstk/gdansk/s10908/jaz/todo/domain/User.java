package pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private static final long serialVersionUID = 7567727213845631241L;

    private String login;
    private String password;
    private List<Task> todo;

    public User() {

    }

    public User(String login, String password, List<Task> todo) {
	super();
	this.login = login;
	this.password = password;
	this.todo = todo;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public List<Task> getTodo() {
	return todo;
    }

    public void setTodo(List<Task> todo) {
	this.todo = todo;
    }

}
