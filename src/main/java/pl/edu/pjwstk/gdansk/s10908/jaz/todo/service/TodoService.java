package pl.edu.pjwstk.gdansk.s10908.jaz.todo.service;

import java.util.Collection;

import javax.ejb.Local;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.Task;

@Local
public interface TodoService {

    public Collection<Task> getUserTaskCollection();

    public boolean addTask(String title, String description);

    public boolean addTask(String priorityName, int priorityValue, String title, String description);

    public boolean removeTask(String taskTitle);

}
