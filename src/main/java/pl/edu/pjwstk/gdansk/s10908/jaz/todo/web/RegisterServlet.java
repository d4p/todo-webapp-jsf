package pl.edu.pjwstk.gdansk.s10908.jaz.todo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.service.AuthenticationService;

@WebServlet(urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {

    private static final long serialVersionUID = 4553032754335411317L;

    @EJB
    AuthenticationService authenticationService;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String errorMessage = authenticationService.register(request.getParameter("login"),
		request.getParameter("password1"), request.getParameter("password2"));
	if (errorMessage != null) {
	    response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    out.print(errorMessage);
	    out.println("Click <a href=\"register.jsp\">here</a> to try again");
	} else {
	    response.sendRedirect(request.getContextPath() + "/todo.jsp");
	}

    }
}
