package pl.edu.pjwstk.gdansk.s10908.jaz.todo.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.Priority;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.Task;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.User;

@Stateless
public class TodoServiceBean implements TodoService {

    @EJB
    private AuthenticationService authenticationService;

    @Override
    public boolean addTask(String title, String description) {
	return addTask("Zwykły", 5, title, description);
    }

    @Override
    public boolean addTask(String priorityName, int priorityValue, String title, String description) {
	User currentUser = authenticationService.getCurrentUser();
	ArrayList<Task> todo = (ArrayList<Task>) currentUser.getTodo();
	return todo.add(new Task(new Priority(priorityName, priorityValue), title, description));
    }

    @Override
    public boolean removeTask(String taskTitle) {
	boolean result = false;
	Collection<Task> todo = authenticationService.getCurrentUser().getTodo();
	for (Task task : todo) {
	    if (task.getTitle().equals(taskTitle)) {
		result = todo.remove(task);
	    }
	}
	return result;
    }

    @Override
    public Collection<Task> getUserTaskCollection() {
	return authenticationService.getCurrentUser().getTodo();
    }
}
