package pl.edu.pjwstk.gdansk.s10908.jaz.todo.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.User;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.service.AuthenticationService;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 8797038918088924054L;

    @EJB
    private AuthenticationService authenticationService;

    private User currentUser;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	currentUser = authenticationService.login(request.getParameter("userLogin"),
		request.getParameter("userPassword"));
	if (currentUser != null) {
	    // HttpSession session = request.getSession();
	    // session.setAttribute("currentUser", currentUser);
	    response.sendRedirect(request.getContextPath() + "/todo.jsp");
	} else {
	    response.sendRedirect(request.getContextPath() + "/login.jsp");
	}
    }

}
