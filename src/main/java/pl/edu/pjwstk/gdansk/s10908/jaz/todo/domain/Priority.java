package pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain;

import java.io.Serializable;

public class Priority implements Serializable {

    private static final long serialVersionUID = 5256196199714060475L;

    private String name;
    private int value;

    public Priority() {
    }

    public Priority(String name, int value) {
	this.name = name;
	this.value = value;
    }

    public String getName() {
	return name;
    }

    public int getValue() {
	return value;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setValue(int value) {
	this.value = value;
    }

    @Override
    public String toString() {
	return "[priority = " + name + "(" + value + ") ]";
    }

}
