package pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain;

import java.io.Serializable;

/**
 * @author damian
 * 
 */
public class Task implements Serializable {

    private static final long serialVersionUID = 4912023157560448149L;

    private String title;
    private String description;
    private Priority priority;

    public Task() {
    }

    public Task(Priority priority, String title, String description) {
	this.priority = priority;
	this.title = title;
	this.description = description;
    }

    public String getTitle() {
	return title;
    }

    public String getDescription() {
	return description;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Priority getPriority() {
	return priority;
    }

    public void setPriority(Priority priority) {
	this.priority = priority;
    }
}
